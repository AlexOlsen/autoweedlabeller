﻿using System.IO;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;

namespace AutoWeedLabeller
{
    /// <summary>
    /// Interaction logic for LoadPopupWindow.xaml
    /// </summary>
    public partial class LoadPopupWindow : Window
    {
        public string path;
        public enum PATH_TYPE
        {
            DIRECTORY, FILE
        }
        public PATH_TYPE pathType;

        public LoadPopupWindow()
        {
            InitializeComponent();
        }

        private void buttonBrowseDirectory_Click(object sender, RoutedEventArgs e)
        {
            // Browse for directory
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                // Validate new selected directory
                string rootDirectory = fbd.SelectedPath;
                validateAutoWeedDirectory(rootDirectory, "The selected path is not a valid directory.",
                    "The selected path is not a valid AutoWeed directory.");
            }
        }

        private void buttonBrowseExistingData_Click(object sender, RoutedEventArgs e)
        {
            // Browse for directory
            OpenFileDialog ofd = new OpenFileDialog();
            
            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                // Validate selected data file directory.
                // Assumes user selected a file with the correct data format.
                string dataFilename = ofd.FileName;
                string rootDirectory = Path.GetDirectoryName(dataFilename);
                if (!WeedLog.IsValidDirectory(rootDirectory))
                {
                    ErrorMessage.Show("The selected data file is not in a valid AutoWeed directory.");
                }
                else
                {
                    path = dataFilename;
                    pathType = PATH_TYPE.FILE;
                    pathText.Content = path;
                }
            }
        }

        private void directoryPathInputBox_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                // Validate new directory
                string rootDirectory = directoryPathInputBox.Text;
                validateAutoWeedDirectory(rootDirectory, "The entered path is not a valid directory.", 
                    "The entered path is not a valid AutoWeed directory.");
            }
        }

        private void validateAutoWeedDirectory(string directory, string invalidDirectoryMessage, string invalidAutoWeedDirectoryMessage)
        {
            if (!Directory.Exists(directory))
            {
                ErrorMessage.Show(invalidDirectoryMessage);
            }
            else if (!WeedLog.IsValidDirectory(directory))
            {
                ErrorMessage.Show(invalidAutoWeedDirectoryMessage);
            }
            else
            {
                path = directory;
                pathType = PATH_TYPE.DIRECTORY;
                pathText.Content = path;
            }
        }

        private void buttonConfirm_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true; // Set result received by Window.ShowDialog()
            Close();
        }

        private void buttonCancel_Click(object sender, RoutedEventArgs e)
        {
            path = null;
            DialogResult = false; // Set result received by Window.ShowDialog()
            Close();
        }
    }
}
