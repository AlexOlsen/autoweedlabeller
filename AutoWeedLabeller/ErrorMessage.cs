﻿
using System.Windows;

namespace AutoWeedLabeller
{
    class ErrorMessage
    {
        public static void Show(string text)
        {
            string caption = "Error";
            MessageBoxButton button = MessageBoxButton.OK;
            MessageBoxImage icon = MessageBoxImage.Error;
            MessageBoxResult result = MessageBox.Show(text, caption, button, icon);
        }
    }
}
