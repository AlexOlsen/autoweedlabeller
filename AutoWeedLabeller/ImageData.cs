using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace AutoWeedLabeller
{
    class ImageData
    {
        public static int DEFAULT_LABEL = -1;
        public string path;
        public int cameraID;
        public int frameID;
        public string filename;
        public List<string> species;
        public List<int> labels;

        public ImageData(string path)
        {
            this.path = path;
            filename = Path.GetFileName(path);
            labels = new List<int>() { DEFAULT_LABEL };
            species = new List<string>();
            cameraID = int.Parse(filename.Substring(17, 1));
            frameID = int.Parse(filename.Substring(5, 6));
        }

        public ImageData(string path, List<string> species, List<int> labels)
        {
            this.path = path;
            filename = Path.GetFileName(path);
            this.species = species;
            this.labels = labels;
            cameraID = int.Parse(filename.Substring(17, 1));
            frameID = int.Parse(filename.Substring(5, 6));
        }

        public ImageData(string path, string speciesString, string labelsString)
        {
            this.path = path;
            filename = Path.GetFileName(path);
            species = getSpeciesFromString(speciesString);
            labels = getLabelsFromString(labelsString);
            cameraID = int.Parse(filename.Substring(17, 1));
            frameID = int.Parse(filename.Substring(5, 6));
        }

        // Returns string of species, suitable for saving to file
        public string getSpeciesString()
        {
            return string.Join(";", species);
        }

        // Returns string of species, suitable for saving to file
        public string getLabelsString()
        {
            if (!labels.Any() || labels[0] == DEFAULT_LABEL || labels.Contains(DEFAULT_LABEL))
            {
                // Set default label string
                return DEFAULT_LABEL.ToString();
            }

            return string.Join(";", labels);
        }

        public List<string> getSpeciesFromString(string speciesString)
        {
            if (speciesString == "")
            {
                //Handle default case (as it will often occur and can be optimised)
                return new List<string>();
            }
            return speciesString.Split(';').ToList();
        }

        public List<int> getLabelsFromString(string labelsString)
        {
            //Handle default case (as it will often occur and can be optimised)
            if (labelsString == DEFAULT_LABEL.ToString())
            {
                return new List<int>() { DEFAULT_LABEL };
            }

            string[] labelStrings = labelsString.Split(';');
            List<int> labels = new List<int>();
            int label = DEFAULT_LABEL;
            for (int i = 0; i < labelStrings.Count(); i++)
            {
                if (int.TryParse(labelStrings.ElementAt(i), out label))
                {
                    labels.Add(label);
                } else {
                    // Invalid label set to only contain default label (which overrides others)
                    labels.Clear();
                    labels.Add(DEFAULT_LABEL);
                    break;
                }
            }

            return labels;
        }
    }
}
