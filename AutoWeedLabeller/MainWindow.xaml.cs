using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.IO;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AutoWeedLabeller
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<string> WEEDS = new List<string>() { "Chinee apple", "Lantana", "Parkinsonia", "Parthenium", "Prickly acacia", "Rubber vine", "Siam weed", "Snake weed", "Negative", "Remove", WeedLog.defaultWEED };
        List<int> LABELS = new List<int>() { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }; //Note: Excludes defaultLABEL from ImageData

        static string CONFIG_FILENAME = "AutoWeedLabellerConfig.txt";

        string rootDirectory;
        WeedLog weedLog;
        int frameCounter;
        int imageCounter; // number of valid images which have been passed (excludes current frame images)
        bool started = false;
        string saveName;
        Image[] images;
        int frame;
        TextBlock[] imageLabels;
        TextBlock[] imageSpeciesLabels;
        Border[] imageBorders;

        public MainWindow()
        {
            InitializeComponent();

            // Initialise label file save name
            saveName = "AutoWeed-" + DateTime.Now.ToString("yyyyMMdd-hhmmss") + ".csv";

            // Check for and load config, use weed species from config, otherwise use builtin as default
            if (File.Exists(CONFIG_FILENAME))
            {
                string fileText = File.ReadAllText(CONFIG_FILENAME);
                List<string> loadedWEEDS = new List<string>(fileText.Split(','));

                // Quick check and removal of final species if only whitespace (or empty)
                string lastString = loadedWEEDS[loadedWEEDS.Count() - 1];
                if (String.IsNullOrWhiteSpace(lastString))
                {
                    loadedWEEDS.RemoveAt(loadedWEEDS.Count() - 1);
                }

                if (loadedWEEDS.Any())
                {
                    // Add Operation 'weeds'
                    loadedWEEDS.Add("Remove");
                    loadedWEEDS.Add(WeedLog.defaultWEED);

                    WEEDS = loadedWEEDS;
                    // Generate labels (includes all weeds but default option)
                    LABELS = Enumerable.Range(0, loadedWEEDS.Count()-1).ToList();
                }
            }
            //Else WEEDS & LABELS use default values in intialiser

            // Initialise weed names in list view
            listSpecies.ItemsSource = WEEDS;

            buttonStart.IsEnabled = false;

            // Create image objects
            images = new Image[4];
            imageLabels = new TextBlock[4];
            imageSpeciesLabels = new TextBlock[4];
            imageBorders = new Border[4];
            for (int i = 0; i < 4; i++)
            {
                images[i] = new Image();
                images[i].Name = string.Format("image{0}", i);
                images[i].MouseDown += Image_MouseDown;

                imageBorders[i] = new Border();
                imageBorders[i].Name = string.Format("imageBorder{0}", i);
                imageBorders[i].Background = Brushes.Transparent;
                imageBorders[i].BorderBrush = Brushes.Transparent;
                imageBorders[i].BorderThickness = new Thickness(2);
                Grid.SetColumn(imageBorders[i], i);
                imageBorders[i].Child = images[i];
                imageGrid.Children.Add(imageBorders[i]);

                imageLabels[i] = new TextBlock();
                imageLabels[i].Name = string.Format("imageLabel{0}", i);
                Grid.SetColumn(imageLabels[i], i);
                labelGrid.Children.Add(imageLabels[i]);

                imageSpeciesLabels[i] = new TextBlock();
                imageSpeciesLabels[i].Name = string.Format("imageSpeciesLabel{0}", i);
                imageSpeciesLabels[i].TextWrapping = TextWrapping.Wrap;
                imageSpeciesLabels[i].TextTrimming = TextTrimming.CharacterEllipsis;
                Grid.SetColumn(imageSpeciesLabels[i], i);
                speciesGrid.Children.Add(imageSpeciesLabels[i]);
            }
        }

        private void Window_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            //Just for testing
            //System.Windows.MessageBox.Show("Pushed button " + e.Key.ToString() + "modifier: " + e.KeyboardDevice.Modifiers.ToString(), "Key Pressed", MessageBoxButton.OK, MessageBoxImage.Information);

            // Check if a number or numpad number (needs numlock on) is pressed
            if ((Key.D0 <= e.Key && e.Key <= Key.D9) || (Key.NumPad0 <= e.Key && e.Key <= Key.NumPad9))
            {
                // Get number of key and check if the number is valid for selecting a weed species option
                // Note: Using 1 to select the first item and 0 to select the 10th item (if any exists).
                int index = getNumberOfKeyPressed(e.Key);
                int itemsCount = listSpecies.Items.Count;
                if (listSpecies.HasItems && ((index != 0 && itemsCount >= index)) || (index == 0 && itemsCount >= 10))
                {
                    //Set selected weed species
                    if (index == 0) {
                        // Set to 10th item
                        listSpecies.SelectedIndex = 9;
                    } else {
                        // Let 1 chose first item etc.
                        listSpecies.SelectedIndex = index - 1;
                    }
                }
            }
        }

        private void Image_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Image image = (Image)sender;
            if (image.Source != null)
            {
                string name = image.Name;
                int id = int.Parse(name.Substring(5, 1));

                List<string> selectedSpecies;
                if (e.ChangedButton == MouseButton.Right && e.ButtonState == MouseButtonState.Pressed)
                {
                    // Reset selected species for image on right click, by using default weed
                    selectedSpecies = new List<string>() { WeedLog.defaultWEED };
                } else
                {
                    // Else load selected species form list, note order is based on order of selection, not items.
                    selectedSpecies = listSpecies.SelectedItems.Cast<string>().ToList();
                }

                if (selectedSpecies.Count() > 0)
                {
                    if (selectedSpecies.Contains(WeedLog.defaultWEED))
                    {
                        imageBorders[id].BorderBrush = Brushes.Transparent;
                    }
                    else
                    {
                        imageBorders[id].BorderBrush = Brushes.Yellow;
                    }

                    int validImagesBeforeSelectedImage = getNumberOfValidImagesBeforeSelectedImage(id);
                    int imageID = imageCounter + validImagesBeforeSelectedImage;
                    weedLog.SetSpecies(imageID, selectedSpecies);
                    bool successful = weedLog.Save();
                    if (!successful)
                    {
                        ErrorMessage.Show("Unable to save to CSV file, check that it isn't in use.");
                    }

                    // Update displayed species label
                    imageSpeciesLabels[id].Text = getSpeciesLabelString(selectedSpecies);
                }
                else
                {
                    ErrorMessage.Show("No labels have been selected.");
                }
            }
        }

        //Generates string used to visualise species associated with image (note space in delimeter allows text wrapping in view)
        private static string getSpeciesLabelString(List<string> selectedSpecies)
        {
            string speciesLabelString;
            if (!selectedSpecies.Any() || selectedSpecies[0] == WeedLog.defaultWEED || selectedSpecies.Contains(WeedLog.defaultWEED))
            {
                // Set default species string (empty)
                speciesLabelString = "";
            }
            else
            {
                speciesLabelString = selectedSpecies[0];
                for (int i = 1; i < selectedSpecies.Count(); i++)
                {
                    speciesLabelString += ", " + selectedSpecies[i];
                }
            }
            return speciesLabelString;
        }

        //Gets number of valid images in the current view before the selected id (0 to 3 range)
        private int getNumberOfValidImagesBeforeSelectedImage(int id)
        {
            //Note: id is also the number of images before the selected image
            int validImagesBeforeSelectedImage = 0;
            for (int i = 0; i < id; i++)
            {
                if (images[i].Source != null)
                {
                    validImagesBeforeSelectedImage++;
                }
            }

            return validImagesBeforeSelectedImage;
        }

        private void buttonBrowse_Click(object sender, RoutedEventArgs e)
        {
            if (!started)
            {
                // Start Load Window Popup to get directory data
                LoadPopupWindow loadPopupWindow = new LoadPopupWindow();
                if (loadPopupWindow.ShowDialog() == true)
                {
                    String path = loadPopupWindow.path;
                    if (path != null)
                    {
                        // Note only valid paths are stored
                        if (loadPopupWindow.pathType == LoadPopupWindow.PATH_TYPE.FILE)
                        {
                            //Load existing data file
                            rootDirectory = System.IO.Path.GetDirectoryName(path);
                            string logName = System.IO.Path.GetFileName(path); // Must not contain path

                            //Note: new WEEDS list is created to remove references to original
                            weedLog = new WeedLog(rootDirectory, logName, true, new List<string>(WEEDS));
                        } else
                        {
                            // Load Directory
                            // Note: new WEEDS list is created to remove references to original
                            rootDirectory = path;
                            weedLog = new WeedLog(path, new List<string>(WEEDS));
                        }

                        frameCounter = 0;
                        imageCounter = 0;
                        buttonStart.IsEnabled = true;
                    }
                }
            }
            else
            {
                // Load previous set of images
                if (frameCounter > 0)
                {
                    frameCounter--;
                    frame = weedLog.frames[frameCounter];
                    loadImages();
                    statusLabel.Text = string.Format("Processing frame {0} ({1}/{2}) for {3}", frame, frameCounter + 1, weedLog.frames.Count(), rootDirectory);
                    progressBar.Value = (int)(100.0 * (frameCounter + 1) / weedLog.frames.Count());

                    for (int i = 0; i < 4; i++)
                    {
                        if (images[i].Source != null)
                        {
                            imageCounter--;
                        }
                    }
                    loadImagesSpecies();
                }
            }
        }

        private void buttonStart_Click(object sender, RoutedEventArgs e)
        {
            if (!started)
            {
                // Start labelling one frame (4 images) at a time
                frameCounter = 0;
                frame = weedLog.frames[frameCounter];
                loadImages();
                loadImagesSpecies();

                started = true;
                statusLabel.Text = string.Format("Processing frame {0} ({1}/{2}) for {3}", frame, frameCounter + 1, weedLog.frames.Count(), rootDirectory);
                buttonStart.Content = "Next";
                buttonBrowse.Content = "Back";
            }
            else
            {
                // Load next set of images
                for (int i = 0; i < 4; i++)
                {
                    if (images[i].Source != null)
                    {
                        imageCounter++;
                    }
                }
                frameCounter++;
                if (frameCounter < weedLog.frames.Count())
                {
                    frame = weedLog.frames[frameCounter];
                    loadImages();
                    loadImagesSpecies();
                    statusLabel.Text = string.Format("Processing frame {0} ({1}/{2}) for {3}", frame, frameCounter + 1, weedLog.frames.Count(), rootDirectory);
                }
                else
                {
                    started = false;
                    buttonStart.Content = "Start";
                    buttonStart.IsEnabled = false;
                    buttonBrowse.Content = "Browse";
                    statusLabel.Text = "Finished processing " + rootDirectory;
                }
            }
            progressBar.Value = (int)(100.0 * (frameCounter + 1) / weedLog.frames.Count());
        }

        private void loadImages()
        {
            BitmapImage[] bitmapImages = new BitmapImage[4];
            for (int i = 0; i < 4; i++)
            {
                string imageName = string.Format("Frame{0:000000}Camera{1}.jpg", frame, i);
                string imagePath = string.Format("{0}\\{1}", rootDirectory, imageName);
                if (File.Exists(imagePath))
                {
                    bitmapImages[i] = new BitmapImage();
                    bitmapImages[i].BeginInit();
                    bitmapImages[i].UriSource = new Uri(imagePath, UriKind.Absolute);
                    bitmapImages[i].EndInit();
                    images[i].Source = bitmapImages[i];
                    imageLabels[i].Text = imageName;
                    imageSpeciesLabels[i].Text = "";
                    imageBorders[i].BorderBrush = Brushes.Transparent;
                }
                else
                {
                    images[i].Source = null;
                    imageLabels[i].Text = "No image from this camera";
                    imageSpeciesLabels[i].Text = "";
                    imageBorders[i].BorderBrush = Brushes.Transparent;
                }
            }
        }

        private void loadImagesSpecies()
        {
            int imageID = imageCounter; // Note: Required to be id for 1st image in current view
            for (int i = 0; i < 4; i++)
            {
                if (images[i].Source != null)
                {
                    string imageSpeciesString = getSpeciesLabelString(weedLog.images[imageID].species);
                    if (imageSpeciesString.Length != 0)
                    {
                        imageBorders[i].BorderBrush = Brushes.Yellow;
                    }
                    imageSpeciesLabels[i].Text = imageSpeciesString;
                    imageID++;
                }
            }
        }

        /* Gets number of a number or numpad key. Returns 0 by default and for 0 keys. */
        private static int getNumberOfKeyPressed(Key key)
        {
            int index = 0;
            switch (key)
            {
                case Key.D0:
                case Key.NumPad0:
                    index = 0;
                    break;
                case Key.D1:
                case Key.NumPad1:
                    index = 1;
                    break;
                case Key.D2:
                case Key.NumPad2:
                    index = 2;
                    break;
                case Key.D3:
                case Key.NumPad3:
                    index = 3;
                    break;
                case Key.D4:
                case Key.NumPad4:
                    index = 4;
                    break;
                case Key.D5:
                case Key.NumPad5:
                    index = 5;
                    break;
                case Key.D6:
                case Key.NumPad6:
                    index = 6;
                    break;
                case Key.D7:
                case Key.NumPad7:
                    index = 7;
                    break;
                case Key.D8:
                case Key.NumPad8:
                    index = 8;
                    break;
                case Key.D9:
                case Key.NumPad9:
                    index = 9;
                    break;
            }
            return index;
        }
    }
}
