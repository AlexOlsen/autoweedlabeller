using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace AutoWeedLabeller
{
    class WeedLog
    {
        public List<ImageData> images;
        string logName;
        string logPath;
        public List<int> frames;
        public static string defaultWEED = "Default"; // option in WEEDS which sets the images weed species to default option (e.g. unselected)
        List<string> WEEDS = new List<string>() { "Chinee apple", "Lantana", "Parkinsonia", "Parthenium", "Prickly acacia", "Rubber vine", "Siam weed", "Snake weed", "Negative", "Remove", defaultWEED };
        List<int> LABELS = new List<int>() { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

        public WeedLog(string path, string logFilename, bool loadData, List<string> weedSpecies)
        {
            logName = logFilename;
            logPath = path + "\\" + logFilename;

            //Handle loading data from file (versus creating new default data)
            List<string> imageDataFilenames = new List<string>();
            List<string> imageDataLabelsStrings = new List<string>();
            List<string> imageDataSpeciesStrings = new List<string>();
            if (loadData) {
                var lines = File.ReadLines(logPath);
                
                // Read lines skipping the header line
                foreach (string line in lines.Skip(1))
                {
                    string[] splittedLine = line.Split(',');
                    imageDataFilenames.Add(splittedLine[0]);
                    imageDataLabelsStrings.Add(splittedLine[1]);
                    imageDataSpeciesStrings.Add(splittedLine[2]);
                }
            }

            //Handle Weed Species
            if (weedSpecies != null && weedSpecies.Any())
            {
                WEEDS = weedSpecies;
                // Generate labels (includes all weeds but default option)
                LABELS = Enumerable.Range(0, weedSpecies.Count() - 1).ToList();
            }
            //Else WEEDS & LABELS use default values in intialiser

            string[] imagePaths = Directory.GetFiles(path, "*.jpg");
            images = new List<ImageData>(imagePaths.Count());
            frames = new List<int>();
            int j = 0;
            for (int i = 0; i < imagePaths.Count(); i++)
            {
                ImageData imageData;
                if (loadData && imageDataFilenames.Count() > i && imagePaths[i] == (path + "\\" + imageDataFilenames[j]))
                {
                    //Use loaded data from file.
                    imageData = new ImageData(imagePaths[i], imageDataSpeciesStrings[j], imageDataLabelsStrings[j]);

                    // Increment imagedata index.
                    // This assumes number of saved datafilenames are always less than or equal the amount in the directory. 
                    // This is always the case if the datafile has been made in the directory and no labelled images have been moved.
                    j++;
                } else
                {
                    imageData = new ImageData(imagePaths[i]);
                }
                images.Add(imageData);

                //Add frame to unique list
                frames.Add(images[i].frameID);
            }
            // Remove duplicates and sort ascending
            frames = frames.Distinct().ToList();
            frames.Sort();
        }

        public WeedLog(string path, string logFilename, bool loadData) : this(path, logFilename, loadData, null)
        { }

        public WeedLog(string path, List<string> weedSpecies) : this(path, "AutoWeed-" + DateTime.Now.ToString("yyyyMMdd-hhmmss") + ".csv", false, weedSpecies)
        { }

        public WeedLog(string path) : this(path, null)
        { }


        public void SetSpecies(int imageID, List<string> species)
        {
            if (species[0] == defaultWEED || species.Contains(defaultWEED))
            {
                // Set default species (empty) and label
                images[imageID].species.Clear();

                images[imageID].labels.Clear();
                images[imageID].labels.Add(ImageData.DEFAULT_LABEL);
            } else {
                // Store labels
                images[imageID].labels.Clear();
                int label;
                for (int i = 0; i < species.Count(); i++)
                {
                    label = WEEDS.FindIndex(x => x.Equals(species[i]));
                    images[imageID].labels.Add(label);
                }

                //Store species
                images[imageID].species.Clear();
                images[imageID].species.AddRange(species);
            }
        }

        public bool Save()
        {
            string text = "Filename,Label,Species";
            for (int i = 0; i < images.Count; i++)
            {
                text += string.Format("{0}{1},{2},{3}", Environment.NewLine, images[i].filename, images[i].getLabelsString(), images[i].getSpeciesString());
            }
            try
            {
                File.WriteAllText(logPath, text);
                return true;
            }
            catch (IOException)
            {
                return false;
            }
        }

        public void Remove(int index)
        {
            File.Delete(images[index].path);
            images.Remove(images[index]);
        }

        public static bool IsValidDirectory(string directory)
        {
            // Valid directory if contains any files with naming convention "Camera%iFrame%i.jpg"
            string[] jpgFiles = Directory.GetFiles(directory, "*.jpg");
            if (jpgFiles.Length == 0)
                return false;
            Random rnd = new Random();
            int r = rnd.Next(jpgFiles.Length);
            string randomFile = Path.GetFileName(jpgFiles[r]);
            if (randomFile.Contains("Camera") || randomFile.Contains("Frame") || randomFile.Contains("jpg"))
                return true;
            else
                return false;
        }
    }
}
